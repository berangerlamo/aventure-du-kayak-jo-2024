#%%
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 09:08:28 2024

@author: lelievr.beranger
"""

################################################################################
#PLAN DU PROGRAMME
#I: Initialisations:
#   1:Fenêtre
#   2:Dictionnaire
#   3:Skins, boutons et objets
#   4:Fonctions
#   5:Variables de Jeu
#
#II: Corps du jeu (Boucle while)#Dans un ordre particulier pour l'affichage
#   1: Phase bataille
#       A:Attaque joueur
#       B:Attaque robot
#       C:Comptage et administratif
#   2:Impressions diverses
#       A: Tableau de bord
#       B: Indicateurs
#       C: Bateaux et grille
#   3: Phase placement
#       A:Placement adversaire
#       B:Placement Joueur
#   4: Phase de fin
#################################################################################



####################
#I: Initialisations#
####################
#import des bibliothèque
import pygame
from sys import *
from random import *

###################################
#1 Initialisation de la fenêtre de jeu
################################
pygame.init()

screen = pygame.display.set_mode((1000,700))
fond=pygame.image.load('graphics\lefond_background_eau_ocean_fondo_fondieren_fondos_lousfoundu.png')

pygame_icon = pygame.image.load('graphics/icone.png')
pygame.display.set_icon(pygame_icon)

pygame.display.set_caption("JO2024: Aventures du Kayak(Bataille Navale)")
Police=pygame.font.Font(None,20)
Police2=pygame.font.Font(None,30)
clock = pygame.time.Clock()

#%%



#######################################
#2  Initialisation des dictionnaires du jeu
#######################################

#cette liste permet d'initialiser les dictionnaires
# qui serviront le fonctionnement du jeu
grille_joueur=[['JA1','JA2','JA3','JA4','JA5','JA6','JA7','JA8','JA9','JA10'],
               ['JB1','JB2','JB3','JB4','JB5','JB6','JB7','JB8','JB9','JB10'],
               ['JC1','JC2','JC3','JC4','JC5','JC6','JC7','JC8','JC9','JC10'],
               ['JD1','JD2','JD3','JD4','JD5','JD6','JD7','JD8','JD9','JD10'],
               ['JE1','JE2','JE3','JE4','JE5','JE6','JE7','JE8','JE9','JE10'],
               ['JF1','JF2','JF3','JF4','JF5','JF6','JF7','JF8','JF9','JF10'],
               ['JG1','JG2','JG3','JG4','JG5','JG6','JG7','JG8','JG9','JG10'],
               ['JH1','JH2','JH3','JH4','JH5','JH6','JH7','JH8','JH9','JH10'],
               ['JI1','JI2','JI3','JI4','JI5','JI6','JI7','JI8','JI9','JI10'],
               ['JJ1','JJ2','JJ3','JJ4','JJ5','JJ6','JJ7','JJ8','JJ9','JJ10']]
# ces dictionnaires vont servir a retrouver les surface
# dessinées ainsi que leur hitbox (rectangle)

grille_joueur_surf={}
grille_joueur_rect={}
grille_joueur_cord={}

#cette liste permet d'initialiser les dictionnaires
# qui serviront le fonctionnement du jeu
grille_adversaire=[['AA1','AA2','AA3','AA4','AA5','AA6','AA7','AA8','AA9','AA10'],
                   ['AB1','AB2','AB3','AB4','AB5','AB6','AB7','AB8','AB9','AB10'],
                   ['AC1','AC2','AC3','AC4','AC5','AC6','AC7','AC8','AC9','AC10'],
                   ['AD1','AD2','AD3','AD4','AD5','AD6','AD7','AD8','AD9','AD10'],
                   ['AE1','AE2','AE3','AE4','AE5','AE6','AE7','AE8','AE9','AE10'],
                   ['AF1','AF2','AF3','AF4','AF5','AF6','AF7','AF8','AF9','AF10'],
                   ['AG1','AG2','AG3','AG4','AG5','AG6','AG7','AG8','AG9','AG10'],
                   ['AH1','AH2','AH3','AH4','AH5','AH6','AH7','AH8','AH9','AH10'],
                   ['AI1','AI2','AI3','AI4','AI5','AI6','AI7','AI8','AI9','AI10'],
                   ['AJ1','AJ2','AJ3','AJ4','AJ5','AJ6','AJ7','AJ8','AJ9','AJ10']]
# ces dictionnaires vont servir a retrouver les surface
# dessinées ainsi que leur hitbox (rectangle)

grille_adversaire_surf={}
grille_adversaire_rect={}
grille_adversaire_cord={}


#ces dictionnaires servent à savoir quelle cases des
#grilles ont deja été touchée
grille_joueur_touchee={"erreur":"erreur"}
grille_adversaire_touchee={"erreur":"erreur"}

#Dictionnaire référençant le nombre de fois qu'un de nos bateaux a été touché
dico_bateaux={'bibonhomme':2,'tribonhomme1':3,'tribonhomme2':3,'quadribonhomme':4,'pentabonhomme':5}
dico_bateaux_adversaire={'bibonhomme':2,'tribonhomme1':3,'tribonhomme2':3,'quadribonhomme':4,'pentabonhomme':5}



#completage des dictionnaires de touchée
for i in range(len(grille_joueur)):
    for j in range(len(grille_joueur[i])):
        grille_joueur_touchee[grille_joueur[i][j]]=[0,None]

for i in range(len(grille_adversaire)):
    for j in range(len(grille_adversaire[i])):
        grille_adversaire_touchee[grille_adversaire[i][j]]=[0,None]


grille_adversaire_attaque={}
grille_joueur_attaque={}
#%%




####################
#3 Definition des boutons, skins et positions
#####################
#Skins Bateaux
bateau1=pygame.image.load("graphics/resize/bibonhomme.png")
bateau2=pygame.image.load("graphics/resize/tribonhomme.png")
bateau3=pygame.image.load("graphics/resize/tribonhomme.png")
bateau4=pygame.image.load("graphics/resize/quadribonhomme.png")
bateau5=pygame.image.load("graphics/resize/pentabonhomme.png")

theme=pygame.mixer.Sound('theme_principal.mp3')
theme.set_volume(12)
theme.stop()
theme.play()

#Interface
tableau_de_bord=pygame.image.load("graphics/Tableau_de_bord.png")
tdb_rect=tableau_de_bord.get_rect(bottomright=(1000,700))

son=pygame.image.load("graphics/interface/son.png")
son_rect=son.get_rect(bottomright=(874,484))

co_radar=(600,675)#Coordonnées radar
radar=pygame.image.load("graphics/animation/radar1.png").convert()
radar_rect=radar.get_rect(bottomright=co_radar)

co_sens=(55,597) # Coordonnées du bouton sens
change_sens=pygame.image.load("graphics/interface/Tourner.png")
reverse=change_sens.get_rect(topleft=co_sens)

co_valid=(135,597) #Coordonnés du bouton validation
validation=pygame.image.load("graphics/interface/Valide.png") 
validation_rect=validation.get_rect(topleft=co_valid)

sortie=pygame.image.load("graphics/exit.png").convert()
sortie_rect=sortie.get_rect(bottomright=(930,675))



aiuto=pygame.image.load("graphics/interface/aiuto.png").convert()
aiuto_rect=aiuto.get_rect(topleft=(215,597))

#Message indiquant les phases
message=pygame.image.load("graphics/message/chargement.png")
message_rect=message.get_rect(topleft=(390,50))


#Voyants
#Bibonhomme
bi_ind=pygame.image.load("graphics/etat/2/2.png")
bi_ind_rect=bi_ind.get_rect(bottomright=(50,485))
#Tri
tri1_ind=pygame.image.load("graphics/etat/3/3.png")
tri1_ind_rect=tri1_ind.get_rect(bottomright=(100,485))

tri2_ind=pygame.image.load("graphics/etat/3/3.png")
tri2_ind_rect=tri2_ind.get_rect(bottomright=(150,485))
#Quadri
qua_ind=pygame.image.load("graphics/etat/4/4.png")
qua_ind_rect=qua_ind.get_rect(bottomright=(200,485))
#Penta
pent_ind=pygame.image.load("graphics/etat/5/5.png")
pent_ind_rect=pent_ind.get_rect(bottomright=(250,485))

#%%



######################################
#4 FONCTIONS
###################################

def calcul_case(case:str,operation:str,valeur:int)->str:
    """
    Fonction complexe pour la manipulation de pointage case:
    renvoie une liste dans laquelle il y des indicateurs de 
    case du type 'JA1', cela permettra de placer algorithmiquement
    les bateaux dans les différents dictionnaires associés

    Parameters
    ----------
    case : str
         Case de base
    operation : str
        Operation à effectuer sous format "-v" ou "+h", direction 
        dans lequel est censé partir le bteau
    valeur : int
        Valeur du calcul, longueur du bateau

    Returns
    -------
    case_bateau : liste
        Nouvelle case pointée, liste dans laquelle on retrouve l'id 
        de chaque case sur lequel est censé se trouver le bateau

    """
    
    case_bateau=[] # cette variable est une liste qui contiendra toute les case demandées selon la direction et la valeur
    caractere_case=[] #cette variable décompose la case de base en trois caractères pour pouvoir les traiter individuelement

    
    for i in case: # décomposition de la case de base
        caractere_case.append(i)
    
    if len(caractere_case)==4: # vérification pour le cas où il y a plus de 3 caractère
        caractere_case[2]=caractere_case[2]+caractere_case[3]

    if operation=="-h":#vérification de la direction
        for i in range(valeur):#détermination de la longueur de la liste via la valeur
            if len(caractere_case)==4:#vérification d'urgence si jamais il y a plus de 4 caractères
                return ["erreur"]
            elif int(caractere_case[2])-i >= 1:#vérification que le bateau placé reste dans la grille
                numero=int(caractere_case[2])-i
                case_bateau.append(caractere_case[0]+caractere_case[1]+str(numero))#ajout de la case dans la liste qui sera renvoyée
            else :
                return ["erreur"]

    elif operation=="+h":#vérification de la direction
        for i in range(valeur):#détermination de la longueur de la liste via la valeur
            if len(caractere_case)==4:#vérification d'urgence si jamais il y a plus de 4 caractères
                return ["erreur"]
            elif int(caractere_case[2])+i <= 10:#vérification que le bateau placé reste dans la grille
                numero=int(caractere_case[2])+i
                case_bateau.append(caractere_case[0]+caractere_case[1]+str(numero))#ajout de la case dans la liste qui sera renvoyée
            else :
                return ["erreur"]

    elif operation=="-v":#vérification de la direction

        traduction_colonnes={"A":1,"B":2,"C":3,"D":4,"E":5,"F":6,"G":7,"H":8,"I":9,"J":10}#remplace les lettre par des 
                                                                                          #chiffres pour simplifier les vérifications

        for i in range(valeur):#détermination de la longueur de la liste via la valeur
            if traduction_colonnes[caractere_case[1]]-i >=1:#vérification que le bateau placé reste dans la grille
                for j in traduction_colonnes.keys():
                    if traduction_colonnes[caractere_case[1]]-i == traduction_colonnes[j]:
                        numero=j
                case_bateau.append(caractere_case[0]+numero+caractere_case[2])#ajout de la case dans la liste qui sera renvoyée
            else :
                return ["erreur"]

    elif operation=="+v":#vérification de la direction

        traduction_colonnes={"A":1,"B":2,"C":3,"D":4,"E":5,"F":6,"G":7,"H":8,"I":9,"J":10}#remplace les lettre par des 
                                                                                          #chiffres pour simplifier les vérifications

        for i in range(valeur):#détermination de la longueur de la liste via la valeur
            if traduction_colonnes[caractere_case[1]]+i <=10:#vérification que le bateau placé reste dans la grille
                for j in traduction_colonnes.keys():
                    if traduction_colonnes[caractere_case[1]]+i == traduction_colonnes[j]:
                        numero=j
                case_bateau.append(caractere_case[0]+numero+caractere_case[2])#ajout de la case dans la liste qui sera renvoyée
            else :
                return ["erreur"]



    return case_bateau




def placement (case,x,y,grille):
    """
    Cette fonction permet de placer une case d'une grille sur la fenêtre et
    ajoute la surface et son rectangle dans deux dictionnaires distincts.
    Cette fonction nous permettra de créer la grille du joueur et de l'adversaire.
    """
    #import des dictionnairess utiles
    global grille_joueur_surf
    global grille_joueur_rect
    global grille_joueur_cord
    global grille_adversaire_surf
    global grille_adversaire_rect
    global grille_adversaire_cord

    if grille=="joueur": #place premièrement dla grille du joueur
        grille_joueur_surf[case] = pygame.Surface((25,25))#initialisation de la surface
        grille_joueur_surf[case].fill('dodgerblue2')#choix de la couleur
        grille_joueur_rect[case]= grille_joueur_surf[case].get_rect(topleft=(x,y))#initialisation du rectangle (hitbox) de la case
        grille_joueur_cord[case]=(x,y)#ajout des coordonnées de la case dans un dictionnaires
        screen.blit(grille_joueur_surf[case],grille_joueur_rect[case])#affichages de la case sur la fenetre

    if grille=="adversaire":#place premièrement dla grille du adversaire
        grille_adversaire_surf[case] = pygame.Surface((25,25))#initialisation de la surface
        grille_adversaire_surf[case].fill('dodgerblue2')#choix de la couleur
        grille_adversaire_rect[case]= grille_adversaire_surf[case].get_rect(topleft=(x,y))#initialisation du rectangle (hitbox) de la case
        grille_adversaire_cord[case]=(x,y)#ajout des coordonnées de la case dans un dictionnaires
        screen.blit(grille_adversaire_surf[case],grille_adversaire_rect[case])#affichages de la case sur la fenetre

#%%





###################
#5 Variables de jeu
###################
placement_adversaire=True #Boucle de placement adversaire
numero_bateaux=1 #Pour le placement
placement_bateaux=0 #Confirmation
placement_bateaux_adversaire=0
sens=0
verif=None #Si placement valable
tempo=0 #Compteur 4 tours
increment_attaque=0 #Différenciation des couleurs pour la pose des cases attaqués
score_joueur=0 #Score joueur par bateau coulé
score_adversaire=0 #Score adversaire par bateau coulé
prochain_coup=[] #Indication IA pour guider les coups
axe_ia=None #Indique l'axe à suivre pour l'IA
score_init_adversaire=0
tour_compte=0#Compteur tour
aide=0

#cette variable sert à savoir si c'est le tour du joueur
# ou de l'adversaire ou si le jeu n'a pas encore commencé
tour="joueur"
jeu='placement'

#%%








#############
#II: Corps du jeu
#############
while True:
        #BASES de l'affichage
       screen.blit(fond,(0,0))
       
       #Scores
       string_score='Score Joueur: '+str(score_joueur)
       score_affichage_joueur=Police.render(str(string_score),False,'White')
       
       string_score='Score Adverse: '+str(score_adversaire)
       score_affichage_adversaire=Police.render(str(string_score),False,'White')
       
       string_score='Tour: '+str(tour_compte)
       tour_compte_affichage=Police2.render(str(string_score),True,'White')
       
       
       screen.blit(score_affichage_adversaire,(870,305))
       screen.blit(score_affichage_joueur,(0,305))
       screen.blit(tour_compte_affichage,(437,120))

                    


#%%
        ####################
        #1: PHASE DE COMBAT#
        ####################
        
       if jeu=='bataille':
            
           
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit()
                if event.type == pygame.MOUSEBUTTONUP:
                    if son_rect.collidepoint(event.pos):
                        theme.set_volume(0.01)
                        
                    if sortie_rect.collidepoint(event.pos):
                        pygame.quit()
                        exit()
                    if aiuto_rect.collidepoint(event.pos):
                        
                        if aide==0:
                            aide=1
                            message=pygame.image.load("graphics/message/aide.png")
                            message_rect=message.get_rect(topleft=(150,0))
                        elif aide==1:
                            aide=0
                            message_rect=message.get_rect(topleft=(390,50))
                            message=pygame.image.load("graphics/message/bataille.png")
                            
                        #attaque du joueur
                    if tour=="joueur":
                        for i in grille_adversaire_rect:
                                    if grille_adversaire_rect[i].collidepoint(event.pos):
                                        if grille_adversaire_touchee[i][0]<2:
                                            if i!=None:
                                                tour="adversaire"
                                                
                                            couleur1='White'                                                                                        
                                            if grille_adversaire_touchee[i][0]==1:
                                                couleur1='Red'
                                            
                                            increment_attaque+=1
                                            grille_adversaire_touchee[i][0]=grille_adversaire_touchee[i][0]+2
                                                                                        
                                            x1=grille_adversaire_rect[i].topleft[0]-2
                                            y1=grille_adversaire_rect[i].topleft[1]-2                                        
                                            grille_adversaire_attaque[increment_attaque]=(pygame.Surface((29,29)),(x1,y1),couleur1)
            




                if tour=="adversaire":
                   if len(prochain_coup)==0:
                       case1=randint(0,9)
                       case2=randint(0,9)
                   else :
                       case1=prochain_coup[0][0]
                       case2=prochain_coup[0][1]
                       prochain_coup.pop(0)
   
                   for i in grille_joueur_rect.keys():
                       if i==grille_joueur[case1][case2]:
                           if grille_joueur_touchee[i][0]<2 :
   
                               couleur2='White'
   
                               if grille_joueur_touchee[i][0]==1:
                                   couleur2='Red'
                                   prochain_coup=[]
                                   if case1 > 0:
                                       prochain_coup.append([case1-1,case2])
                                   if case1 < 9:
                                       prochain_coup.append([case1+1,case2])
                                   if case2 > 0:
                                       prochain_coup.append([case1,case2-1])
                                   if case2 < 9:
                                       prochain_coup.append([case1,case2+1])
                                   shuffle(prochain_coup)
   
                               grille_joueur_touchee[i][0]=grille_joueur_touchee[i][0]+2
   
                               x2=grille_joueur_rect[i].topleft[0]-2
                               y2=grille_joueur_rect[i].topleft[1]-2
   
                               grille_joueur_attaque[increment_attaque]=(pygame.Surface((29,29)),(x2,y2),couleur2)
                               tour_compte=tour_compte+1
                               tour="comptage"
                       

                if tour=="comptage":
                    pygame.time.delay(20)
                    #Verification touché-coulé joueur+indicateurs
                    for i in grille_joueur_touchee:
                        if grille_joueur_touchee[i][0]==3:
                            for j in dico_bateaux:
                                if j==grille_joueur_touchee[i][1]:
                                    dico_bateaux[j]-=1
                                    grille_joueur_touchee[i][0]=4
                    #Verification touché-coulé adversaire
                    for i in grille_adversaire_touchee:
                        if grille_adversaire_touchee[i][0]==3:
                            for j in dico_bateaux:
                                if j==grille_adversaire_touchee[i][1]:
                                    dico_bateaux_adversaire[j]-=1
                                    grille_adversaire_touchee[i][0]=4
                        
                        tour="joueur"
                            
                            
#%%


        ######################
        #2: IMPRESSIONS DIVERSES#
        ######################
        
        
        
        
        #Animation quatre temps:
       if jeu=="bataille" or jeu=='placement':
           if tempo>60:
               radar=pygame.image.load("graphics/animation/radar1.png").convert()
               radar_rect=radar.get_rect(bottomright=co_radar)
    
    
               if tempo>120:
                   radar=pygame.image.load("graphics/animation/radar2.png").convert()
                   radar_rect=radar.get_rect(bottomright=co_radar)
    
                   if tempo>180:
                       radar=pygame.image.load("graphics/animation/radar3.png").convert()
                       radar_rect=radar.get_rect(bottomright=co_radar)
    
           else:
               radar=pygame.image.load("graphics/animation/radar4.png").convert()
               radar_rect=radar.get_rect(bottomright=co_radar)

       #Impression du tableau de bord et radar
       screen.blit(tableau_de_bord,tdb_rect)
       screen.blit(radar,radar_rect)
       
       ###########
       #Indicateurs
       ############
       if jeu=="bataille" or jeu=='placement':
           if dico_bateaux['bibonhomme']==1:
               bi_ind=pygame.image.load("graphics/etat/2/1.png")
           elif dico_bateaux['bibonhomme']==0:
               bi_ind=pygame.image.load("graphics/etat/2/2.png")
               score_adversaire+=1
               dico_bateaux['bibonhomme']=-1
               
           if dico_bateaux['tribonhomme1']==2:
               tri1_ind=pygame.image.load("graphics/etat/3/1.png")
           elif dico_bateaux['tribonhomme1']==1:
               tri1_ind=pygame.image.load("graphics/etat/3/2.png")
           elif dico_bateaux['tribonhomme1']==0:
               tri1_ind=pygame.image.load("graphics/etat/3/3.png")
               score_adversaire+=1
               dico_bateaux['tribonhomme1']=-1
               
               
           if dico_bateaux['tribonhomme2']==2:
               tri2_ind=pygame.image.load("graphics/etat/3/1.png")
           elif dico_bateaux['tribonhomme2']==1:
               tri2_ind=pygame.image.load("graphics/etat/3/2.png")
           elif dico_bateaux['tribonhomme2']==0:
               tri2_ind=pygame.image.load("graphics/etat/3/3.png")
               score_adversaire+=1
               dico_bateaux['tribonhomme2']=-1
               
           if dico_bateaux['quadribonhomme']==3:
               qua_ind=pygame.image.load("graphics/etat/4/1.png")
           elif dico_bateaux['quadribonhomme']==2:
               qua_ind=pygame.image.load("graphics/etat/4/2.png")
           elif dico_bateaux['quadribonhomme']==1:
               qua_ind=pygame.image.load("graphics/etat/4/3.png")
           elif dico_bateaux['quadribonhomme']==0:
               qua_ind=pygame.image.load("graphics/etat/4/4.png")
               score_adversaire+=1
               dico_bateaux['quadribonhomme']=-1
               
           if dico_bateaux['pentabonhomme']==4:
               pent_ind=pygame.image.load("graphics/etat/5/1.png")
           elif dico_bateaux['pentabonhomme']==3:
               pent_ind=pygame.image.load("graphics/etat/5/2.png")
           elif dico_bateaux['pentabonhomme']==2:
               pent_ind=pygame.image.load("graphics/etat/5/3.png")
           elif dico_bateaux['pentabonhomme']==1:
               pent_ind=pygame.image.load("graphics/etat/5/4.png")
           elif dico_bateaux['pentabonhomme']==0:
               pent_ind=pygame.image.load("graphics/etat/5/5.png")
               score_adversaire+=1
               dico_bateaux['pentabonhomme']=-1
               
               
               
           if dico_bateaux_adversaire['pentabonhomme']==0:
               score_joueur+=1
               dico_bateaux_adversaire['pentabonhomme']=-1
               
               
           if dico_bateaux_adversaire['quadribonhomme']==0:
               score_joueur+=1
               dico_bateaux_adversaire['quadribonhomme']=-1
               
           if dico_bateaux_adversaire['tribonhomme1']==0:
               score_joueur+=1
               dico_bateaux_adversaire['tribonhomme1']=-1
               
           if dico_bateaux_adversaire['tribonhomme2']==0:
               score_joueur+=1
               dico_bateaux_adversaire['tribonhomme2']=-1
               
               
           if dico_bateaux_adversaire['bibonhomme']==0:
               score_joueur+=1
               dico_bateaux_adversaire['bibonhomme']=-1
            
           #Impression des indicateurs
           screen.blit(bi_ind,bi_ind_rect)
           screen.blit(tri1_ind,tri1_ind_rect)
           screen.blit(tri2_ind,tri2_ind_rect)
           screen.blit(qua_ind,qua_ind_rect)
           screen.blit(pent_ind,pent_ind_rect)
       #########

#%%
       if jeu=="bataille" or jeu=='placement':
           
           #placement de la grille du joueur
           b=-22
           for i in range(len(grille_joueur)):
               b=b+30
               a=-22
               for j in range(len(grille_joueur[i])):
                   a=a+30
                   placement(grille_joueur[i][j],a,b,"joueur")
    
           #placement de la grille de l'adversaire
           b=-22
           for i in range(len(grille_adversaire)):
               b=b+30
               a=670
               for j in range(len(grille_adversaire[i])):
                   a=a+30
                   placement(grille_adversaire[i][j],a,b,"adversaire")
#%%
       #Impression exit
       screen.blit(sortie,sortie_rect)
       #Impression bouton
       screen.blit(change_sens,reverse)
       screen.blit(validation,validation_rect)
       screen.blit(aiuto,aiuto_rect)
       screen.blit(son,son_rect)
       
       
       #placement des rectangles blancs ou rouges representant les attaques (selon les coordonnées de la liste)
       if jeu=="bataille" or jeu=='placement':
           for i in grille_adversaire_attaque.keys():
               grille_adversaire_attaque[i][0].fill(grille_adversaire_attaque[i][2])
               screen.blit(grille_adversaire_attaque[i][0],grille_adversaire_attaque[i][1])
               
           for i in grille_joueur_attaque.keys():
               grille_joueur_attaque[i][0].fill(grille_joueur_attaque[i][2])
               screen.blit(grille_joueur_attaque[i][0],grille_joueur_attaque[i][1])
          
       #Placement des bateaux si placé et selon coordonnées
           if placement_bateaux >= 1:
               screen.blit(bateau1_horizontale,bateau1_rect)
    
           if placement_bateaux >= 2:
               screen.blit(bateau2_horizontale,bateau2_rect)
    
           if placement_bateaux >= 3:
               screen.blit(bateau3_horizontale,bateau3_rect)
               
           if placement_bateaux >= 4:
               screen.blit(bateau4_horizontale,bateau4_rect)
               
           if placement_bateaux == 5:
               screen.blit(bateau5_horizontale,bateau5_rect)
       
       
       #############################
       #3: PHASE DE DEBUT/PLACEMENT#
       #############################
       #Deplacée en bas pour permettre une meilleur impression
       if jeu=='placement':
           
           
           #################
           #A: PLACEMENT ADVERSAIRE
           #################
           
           
           while placement_adversaire==True:
               pygame.time.delay(1)
               message=pygame.image.load("graphics/message/chargement.png")
               message_rect=message.get_rect(topleft=(390,50))
               #Choix aleatoire d'une case
               case1=randint(0,9)
               case2=randint(1,10)
               #Conversion en format du dictionnaire
               pose_adversaire='A'+chr(65+case1)+str(case2)
               
               #BATEAU 1
               if numero_bateaux==1: #Bateau 1 (bibonhomme) en placement
                   ran_sens=randint(0,1) #choix du sens
                   if ran_sens==0:#Cas horizontal
                       #Calcul des cases sur lequel se trouve le bateau
                       liste_case=calcul_case(pose_adversaire,"-v",2)
                       
                        #Verif validité des cases (en grille et non superposition)
                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=1
                           

                   if ran_sens==1:#Cas Vertical
                       liste_case=calcul_case(pose_adversaire,"+h",2)

                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=1

                    #Validation placement
                   if placement_bateaux_adversaire==1:
                               for j in liste_case:
                                   if type(grille_adversaire_touchee[j][0])==str:
                                    break
                                   grille_adversaire_touchee[j][0]=1 #Affirme la presence d'un bateau sur la case
                                   grille_adversaire_touchee[j][1]='bibonhomme' #Nomme le bateau sur la case
                               numero_bateaux=2 #Passage au suivant



                #BATEAU 2
               if numero_bateaux==2:#même fonctionnement que pour le premier bateau
                  ran_sens=randint(0,1)
                  if ran_sens==0:
                      liste_case=calcul_case(pose_adversaire,"-v",3)
                      for j in liste_case:
                          if grille_adversaire_touchee[j][0]==0:
                              verif=True
                          elif grille_adversaire_touchee[j][0]=="erreur":
                              verif=False
                              break
                          else:
                              verif=False
                              break
                      if verif==True:
                           placement_bateaux_adversaire=2
                  if ran_sens==1:
                      liste_case=calcul_case(pose_adversaire,"+h",3)
                      for j in liste_case:
                          if grille_adversaire_touchee[j][0]==0:
                              verif=True
                          elif grille_adversaire_touchee[j][0]=="erreur":
                              verif=False
                              break
                          else:
                              verif=False
                              break
                      if verif==True:
                           placement_bateaux_adversaire=2
                  if placement_bateaux_adversaire==2:
                              for j in liste_case:
                                  if type(grille_adversaire_touchee[j][0])==str:
                                   break
                                  grille_adversaire_touchee[j][0]=1
                                  grille_adversaire_touchee[j][1]='tribonhomme1'
                              numero_bateaux=3
                #BATEAU 3
               if numero_bateaux==3:#même fonctionnement que pour le premier bateau
                   ran_sens=randint(0,1)
                   if ran_sens==0:
                       liste_case=calcul_case(pose_adversaire,"-v",3)
                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=3
                   if ran_sens==1:
                       liste_case=calcul_case(pose_adversaire,"+h",3)

                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=3
                   if placement_bateaux_adversaire==3:
                               for j in liste_case:
                                   if type(grille_adversaire_touchee[j][0])==str:
                                    break
                                   grille_adversaire_touchee[j][0]=1
                                   grille_adversaire_touchee[j][1]='tribonhomme2'
                               numero_bateaux=4

                #BATEAU 4
               if numero_bateaux==4:#même fonctionnement que pour le premier bateau
                   ran_sens=randint(0,1)
                   if ran_sens==0:
                       liste_case=calcul_case(pose_adversaire,"-v",4)
                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=4
                   if ran_sens==1:
                       liste_case=calcul_case(pose_adversaire,"+h",4)
                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=4
                   if placement_bateaux_adversaire==4:
                               for j in liste_case:
                                   if type(grille_adversaire_touchee[j][0])==str:
                                    break
                                   grille_adversaire_touchee[j][0]=1
                                   grille_adversaire_touchee[j][1]='quadribonhomme'
                               numero_bateaux=5
                               
                #BATEAU 5
               if numero_bateaux==5:#même fonctionnement que pour le premier bateau
                   ran_sens=randint(0,1)
                   if ran_sens==0:
                       liste_case=calcul_case(pose_adversaire,"-v",5)
                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=5
                   if ran_sens==1:
                       liste_case=calcul_case(pose_adversaire,"+h",5)

                       for j in liste_case:
                           if grille_adversaire_touchee[j][0]==0:
                               verif=True
                           elif grille_adversaire_touchee[j][0]=="erreur":
                               verif=False
                               break
                           else:
                               verif=False
                               break
                       if verif==True:
                            placement_bateaux_adversaire=5
                   if placement_bateaux_adversaire==5:
                               for j in liste_case:
                                   if type(grille_adversaire_touchee[j][0])==str:
                                    break
                                   grille_adversaire_touchee[j][0]=1
                                   grille_adversaire_touchee[j][1]='pentabonhomme'
                               numero_bateaux=1
                               message=pygame.image.load("graphics/message/placement.png")
                               placement_adversaire=False
                               
                                                   
           
            
           
           
#%%           
           #####################
           #B: PLACEMENT JOUEUR
           #####################
           
           
           for event in pygame.event.get():
               if event.type == pygame.QUIT:
                   pygame.quit()
                   exit()
                   
                #Prévisualisation du bateau au niveau de la souris
               if event.type == pygame.MOUSEMOTION:
                    if numero_bateaux==1:
                        if sens==0:
                            bateau1_affichage=bateau1
                            screen.blit(bateau1_affichage,(pygame.mouse.get_pos()[0]-10,pygame.mouse.get_pos()[1]-50))
                        if sens==1:
                            bateau1_affichage=pygame.transform.rotate(bateau1,270)
                            screen.blit(bateau1_affichage,pygame.mouse.get_pos())
                    if numero_bateaux==2:
                         if sens==0:
                             bateau2_affichage=bateau2
                             screen.blit(bateau2_affichage,(pygame.mouse.get_pos()[0]-10,pygame.mouse.get_pos()[1]-75))
                         if sens==1:
                             bateau2_affichage=pygame.transform.rotate(bateau2,270)
                             screen.blit(bateau2_affichage,pygame.mouse.get_pos())
                    if numero_bateaux==3:
                         if sens==0:
                             bateau3_affichage=bateau3
                             screen.blit(bateau3_affichage,(pygame.mouse.get_pos()[0]-10,pygame.mouse.get_pos()[1]-75))
                         if sens==1:
                             bateau3_affichage=pygame.transform.rotate(bateau3,270)
                             screen.blit(bateau3_affichage,pygame.mouse.get_pos())
                    if numero_bateaux==4:
                          if sens==0:
                              bateau4_affichage=bateau4
                              screen.blit(bateau4_affichage,(pygame.mouse.get_pos()[0]-10,pygame.mouse.get_pos()[1]-100))
                          if sens==1:
                              bateau4_affichage=pygame.transform.rotate(bateau4,270)
                              screen.blit(bateau4_affichage,pygame.mouse.get_pos())
                    if numero_bateaux==5:
                          if sens==0:
                              bateau5_affichage=bateau5
                              screen.blit(bateau5_affichage,(pygame.mouse.get_pos()[0]-10,pygame.mouse.get_pos()[1]-125))
                          if sens==1:
                              bateau5_affichage=pygame.transform.rotate(bateau5,270)
                              screen.blit(bateau5_affichage,(pygame.mouse.get_pos()[0]-10,pygame.mouse.get_pos()[1]))
                   
                        
               if event.type == pygame.MOUSEBUTTONUP:
                   if son_rect.collidepoint(event.pos):
                       theme.set_volume(0.01)
                   if sortie_rect.collidepoint(event.pos):
                       pygame.quit()
                       exit()
                   if  reverse.collidepoint(event.pos):
                       change_sens=pygame.image.load("graphics/interface/Tourner2.png")
                       reverse=change_sens.get_rect(topleft=co_sens)
                       if sens==0:
                           sens=1
                       elif sens==1:
                           sens=0
    
                   if aiuto_rect.collidepoint(event.pos):
                       
                       if aide==0:
                           aide=1
                           message=pygame.image.load("graphics/message/aide.png")
                           message_rect=message.get_rect(topleft=(150,0))
                       elif aide==1:
                           aide=0
                           message_rect=message.get_rect(topleft=(390,50))
                           message=pygame.image.load("graphics/message/placement.png")
                               
                               

                   for i in grille_joueur_rect:
                       if numero_bateaux==1:                                                                                 
                           if grille_joueur_rect[i].collidepoint(event.pos):#parcour la grille pour savoir où le joueur clique
                               #initialisation des images des boutons fonctionnels
                                   validation=pygame.image.load("graphics/interface/Valide.png")
                                   change_sens=pygame.image.load("graphics/interface/Tourner.png")
                                   
                                   id_case=i #trouve l'id de la case choisi par le joueur (type 'JA1')


                                   if sens==0:#cas vertical
                                       liste_case=calcul_case(id_case,"-v",2) #calcul de la position du futur bateau


                                       for j in liste_case: #vérification que le bateau est bien positionné sur la grille

                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                       if verif==True:
                                           #définition de la texture du bateau et de sa postion sur la grille
                                           coord=(grille_joueur_cord[i][0]+2,grille_joueur_cord[i][1]-3)
                                           bateau1_rect=bateau1.get_rect(midleft=coord)
                                           bateau1_horizontale=bateau1
                                           placement_bateaux=1
                                           


                                   if sens==1:#cas horizontal
                                       liste_case=calcul_case(id_case,"+h",2) #calcul de la position du futur bateau

                                       for j in liste_case:#vérification que le bateau est bien positionné sur la grille
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                       if verif==True:
                                           #définition de la texture du bateau et de sa postion sur la grille
                                           coord=(grille_joueur_cord[i][0]+2,grille_joueur_cord[i][1]+28)
                                           bateau1_rect=bateau1.get_rect(midleft=coord)
                                           bateau1_horizontale=pygame.transform.rotate(bateau1,270)
                                           placement_bateaux=1


                           if validation_rect.collidepoint(event.pos) and placement_bateaux==1: #vérifi si le joueur à validé son placement
                                       for j in liste_case:
                                           if type(grille_joueur_touchee[j][0])==str:#vérification d'urgence
                                            break
                                            #placement algorithmique dans le dictionnaire grille_joueur_touchee
                                           grille_joueur_touchee[j][0]=1
                                           grille_joueur_touchee[j][1]='bibonhomme'
                                       #chagement de texture du bouton de validation
                                       validation=pygame.image.load("graphics/interface/Valide2.png")
                                       validation_rect=validation.get_rect(topleft=co_valid)
                                       #passage au placement du second bateau
                                       numero_bateaux=2
                                       #affichage de la barre de vie du bateau
                                       bi_ind=pygame.image.load("graphics/etat/2/0.png")
                                       




                       if numero_bateaux==2:#même fonctionnement que pour le premier bateau

                          for i in grille_joueur_rect:
                              if grille_joueur_rect[i].collidepoint(event.pos):
                                  validation=pygame.image.load("graphics/interface/Valide.png")
                                  validation_rect=validation.get_rect(topleft=co_valid)
                                  change_sens=pygame.image.load("graphics/interface/Tourner.png")
                                  if grille_joueur_touchee[i][0]==0:

                                      id_case=i

                                     #Cas Vertical
                                      if sens==0:

                                       liste_case=calcul_case(id_case,"-v",3)

                                       for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                       if verif==True:
                                          coord=(grille_joueur_cord[i][0]+2,grille_joueur_cord[i][1]+25)
                                          bateau2_rect=bateau2.get_rect(bottomleft=coord)
                                          bateau2_horizontale=bateau2
                                          placement_bateaux=2

                                    #Cas Horizontal
                                      if sens==1:

                                       liste_case=calcul_case(id_case,"+h",3)

                                       for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                       if verif==True:
                                          coord=(grille_joueur_cord[i][0],grille_joueur_cord[i][1]+2)
                                          bateau2_rect=bateau2.get_rect(topleft=coord)
                                          bateau2_horizontale=pygame.transform.rotate(bateau2,270)
                                          placement_bateaux=2

                              if validation_rect.collidepoint(event.pos)and placement_bateaux==2:
                                  for j in liste_case:
                                        if type(grille_joueur_touchee[j][0])==str:
                                            break
                                        grille_joueur_touchee[j][0]=1
                                        grille_joueur_touchee[j][1]='tribonhomme1'
                                  numero_bateaux=3
                                  validation=pygame.image.load("graphics/interface/Valide2.png")
                                  validation_rect=validation.get_rect(topleft=co_valid)
                                  tri1_ind=pygame.image.load("graphics/etat/3/0.png")




                       if numero_bateaux==3:#même fonctionnement que pour le premier bateau

                           for i in grille_joueur_rect:
                               if grille_joueur_rect[i].collidepoint(event.pos):
                                   validation=pygame.image.load("graphics/interface/Valide.png")
                                   change_sens=pygame.image.load("graphics/interface/Tourner.png")
                                   validation_rect=validation.get_rect(topleft=co_valid)
                                   if grille_joueur_touchee[i][0]==0:
                                       #Cas vertical
                                       id_case=i

                                       if sens==0:

                                         liste_case=calcul_case(id_case,"-v",3)

                                         for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                         if verif==True:

                                           coord=(grille_joueur_cord[i][0]+2,grille_joueur_cord[i][1]+25)
                                           bateau3_rect=bateau3.get_rect(bottomleft=coord)
                                           bateau3_horizontale=bateau3
                                           placement_bateaux=3

                                    #Cas Horizontal
                                       if sens==1:

                                         liste_case=calcul_case(id_case,"+h",3)

                                         for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                         if verif==True:
                                           coord=(grille_joueur_cord[i][0],grille_joueur_cord[i][1]+2)
                                           bateau3_rect=bateau3.get_rect(topleft=coord)
                                           bateau3_horizontale=pygame.transform.rotate(bateau3,270)
                                           placement_bateaux=3

                               if validation_rect.collidepoint(event.pos)and placement_bateaux==3:
                                   for j in liste_case:
                                           if type(grille_joueur_touchee[j][0])==str:
                                            break
                                           grille_joueur_touchee[j][0]=1
                                           grille_joueur_touchee[j][1]='tribonhomme2'
                                   numero_bateaux=4
                                   validation=pygame.image.load("graphics/interface/Valide2.png")
                                   validation_rect=validation.get_rect(topleft=co_valid)
                                   tri2_ind=pygame.image.load("graphics/etat/3/0.png")

                       if numero_bateaux==4:#même fonctionnement que pour le premier bateau

                           for i in grille_joueur_rect:
                               if grille_joueur_rect[i].collidepoint(event.pos):
                                   
                                   change_sens=pygame.image.load("graphics/interface/Tourner.png")
                                   validation=pygame.image.load("graphics/interface/Valide.png")
                                   validation_rect=validation.get_rect(topleft=co_valid)
                                   if grille_joueur_touchee[i][0]==0:
                                       #Cas vertical
                                       id_case=i

                                       if sens==0:

                                         liste_case=calcul_case(id_case,"-v",4)

                                         for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                         if verif==True:

                                           coord=(grille_joueur_cord[i][0]+2,grille_joueur_cord[i][1]+25)
                                           bateau4_rect=bateau4.get_rect(bottomleft=coord)
                                           bateau4_horizontale=bateau4
                                           placement_bateaux=4

                                    #Cas Horizontal
                                       if sens==1:

                                         liste_case=calcul_case(id_case,"+h",4)

                                         for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                         if verif==True:
                                           coord=(grille_joueur_cord[i][0],grille_joueur_cord[i][1]+2)
                                           bateau4_rect=bateau4.get_rect(topleft=coord)
                                           bateau4_horizontale=pygame.transform.rotate(bateau4,270)
                                           placement_bateaux=4

                               if validation_rect.collidepoint(event.pos)and placement_bateaux==4:
                                   for j in liste_case:
                                           if type(grille_joueur_touchee[j][0])==str:
                                            break
                                           grille_joueur_touchee[j][0]=1
                                           grille_joueur_touchee[j][1]='quadribonhomme'
                                   numero_bateaux=5
                                   validation=pygame.image.load("graphics/interface/Valide2.png")
                                   validation_rect=validation.get_rect(topleft=co_valid)
                                   qua_ind=pygame.image.load("graphics/etat/4/0.png")

                       if numero_bateaux==5:#même fonctionnement que pour le premier bateau

                           for i in grille_joueur_rect:
                               if grille_joueur_rect[i].collidepoint(event.pos):
                                   
                                   change_sens=pygame.image.load("graphics/interface/Tourner.png")
                                   validation=pygame.image.load("graphics/interface/Valide.png")
                                   validation_rect=validation.get_rect(topleft=co_valid)
                                   if grille_joueur_touchee[i][0]==0:
                                       #Cas vertical
                                       id_case=i

                                       if sens==0:

                                         liste_case=calcul_case(id_case,"-v",5)

                                         for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                         if verif==True:

                                           coord=(grille_joueur_cord[i][0]+2,grille_joueur_cord[i][1]+25)
                                           bateau5_rect=bateau5.get_rect(bottomleft=coord)
                                           bateau5_horizontale=bateau5
                                           placement_bateaux=5

                                    #Cas Horizontal
                                       if sens==1:

                                         liste_case=calcul_case(id_case,"+h",5)

                                         for j in liste_case:
                                           if grille_joueur_touchee[j][0]==0:
                                               verif=True
                                           elif grille_joueur_touchee[j][0]=="erreur":
                                               verif=False
                                               break
                                           else:
                                               verif=False
                                               break

                                         if verif==True:
                                           coord=(grille_joueur_cord[i][0],grille_joueur_cord[i][1]+2)
                                           bateau5_rect=bateau5.get_rect(topleft=coord)
                                           bateau5_horizontale=pygame.transform.rotate(bateau5,270)
                                           placement_bateaux=5

                               if validation_rect.collidepoint(event.pos)and placement_bateaux==5:
                                   
                                   for j in liste_case:
                                           if type(grille_joueur_touchee[j][0])==str:
                                            break
                                           grille_joueur_touchee[j][0]=1
                                           grille_joueur_touchee[j][1]='pentabonhomme'
                                   jeu='bataille'
                                   validation=pygame.image.load("graphics/interface/Valide2.png")
                                   validation_rect=validation.get_rect(topleft=co_valid)
                                   pent_ind=pygame.image.load("graphics/etat/5/0.png")
                                   message=pygame.image.load("graphics/message/bataille.png") #CHangement affichage
                                   break
       
       #Impression de l'affichage global
       screen.blit(message,message_rect)

       #################
       #4: PHASE DE FIN#
       #################
       #Verification fin du jeu
       if score_adversaire==5:
           message=pygame.image.load("graphics/message/fin_ordi.png")
           message_rect=message.get_rect(topleft=(150,0))
           pygame.time.delay(500)
           score_adversaire=0
           jeu="menu"
           
       if score_joueur==5:
           message=pygame.image.load("graphics/message/fin_joueur.png")
           message_rect=message.get_rect(topleft=(150,0))
           pygame.time.delay(500)
           score_joueur=0
           jeu="menu"
           
           
       comptage_magique_verification=0#Verification d'urgence temporaire 
       if score_joueur>=4:
           for i in grille_adversaire_touchee.values():
               if i[0]==1:
                   comptage_magique_verification+=1 
           if comptage_magique_verification==0:
               message=pygame.image.load("graphics/message/fin_joueur.png")
               message_rect=message.get_rect(topleft=(150,0))
               pygame.time.delay(500)
               score_joueur=0
               jeu="menu"
               
                                                                            
       if jeu=='menu':
           sortie2=pygame.image.load("graphics/interface/sortie.png")
           sortie2_rect=sortie2.get_rect(topleft=(750,250))
           rematch=pygame.image.load("graphics/interface/rematch.png")
           rematch_rect=rematch.get_rect(topleft=(150,250))
           for event in pygame.event.get():
               if event.type == pygame.QUIT:
                   pygame.quit()
                   exit()
               if event.type == pygame.MOUSEBUTTONUP:
                   if sortie_rect.collidepoint(event.pos) or sortie2_rect.collidepoint(event.pos):
                       pygame.quit()
                       exit()
#%%
                   if rematch_rect.collidepoint(event.pos):
                       ################################
                       #REINITIALISATION DES VALEURS DU JEU
                       #################################
                       
                       
                       
                       #######################################
                       # Initialisation des dictionnaires du jeu
                       #######################################

                       #cette liste permet d'initialiser les dictionnaires
                       # qui serviront le fonctionnement du jeu
                       grille_joueur=[['JA1','JA2','JA3','JA4','JA5','JA6','JA7','JA8','JA9','JA10'],
                                      ['JB1','JB2','JB3','JB4','JB5','JB6','JB7','JB8','JB9','JB10'],
                                      ['JC1','JC2','JC3','JC4','JC5','JC6','JC7','JC8','JC9','JC10'],
                                      ['JD1','JD2','JD3','JD4','JD5','JD6','JD7','JD8','JD9','JD10'],
                                      ['JE1','JE2','JE3','JE4','JE5','JE6','JE7','JE8','JE9','JE10'],
                                      ['JF1','JF2','JF3','JF4','JF5','JF6','JF7','JF8','JF9','JF10'],
                                      ['JG1','JG2','JG3','JG4','JG5','JG6','JG7','JG8','JG9','JG10'],
                                      ['JH1','JH2','JH3','JH4','JH5','JH6','JH7','JH8','JH9','JH10'],
                                      ['JI1','JI2','JI3','JI4','JI5','JI6','JI7','JI8','JI9','JI10'],
                                      ['JJ1','JJ2','JJ3','JJ4','JJ5','JJ6','JJ7','JJ8','JJ9','JJ10']]
                       # ces dictionnaires vont servir a retrouver les surface
                       # dessinées ainsi que leur hitbox (rectangle)

                       grille_joueur_surf={}
                       grille_joueur_rect={}
                       grille_joueur_cord={}

                       #cette liste permet d'initialiser les dictionnaires
                       # qui serviront le fonctionnement du jeu
                       grille_adversaire=[['AA1','AA2','AA3','AA4','AA5','AA6','AA7','AA8','AA9','AA10'],
                                          ['AB1','AB2','AB3','AB4','AB5','AB6','AB7','AB8','AB9','AB10'],
                                          ['AC1','AC2','AC3','AC4','AC5','AC6','AC7','AC8','AC9','AC10'],
                                          ['AD1','AD2','AD3','AD4','AD5','AD6','AD7','AD8','AD9','AD10'],
                                          ['AE1','AE2','AE3','AE4','AE5','AE6','AE7','AE8','AE9','AE10'],
                                          ['AF1','AF2','AF3','AF4','AF5','AF6','AF7','AF8','AF9','AF10'],
                                          ['AG1','AG2','AG3','AG4','AG5','AG6','AG7','AG8','AG9','AG10'],
                                          ['AH1','AH2','AH3','AH4','AH5','AH6','AH7','AH8','AH9','AH10'],
                                          ['AI1','AI2','AI3','AI4','AI5','AI6','AI7','AI8','AI9','AI10'],
                                          ['AJ1','AJ2','AJ3','AJ4','AJ5','AJ6','AJ7','AJ8','AJ9','AJ10']]
                       # ces dictionnaires vont servir a retrouver les surface
                       # dessinées ainsi que leur hitbox (rectangle)

                       grille_adversaire_surf={}
                       grille_adversaire_rect={}
                       grille_adversaire_cord={}


                       #ces dictionnaires servent à savoir quelle cases des
                       #grilles ont deja été touchée
                       grille_joueur_touchee={"erreur":"erreur"}
                       grille_adversaire_touchee={"erreur":"erreur"}

                       #Dictionnaire référençant le nombre de fois qu'un de nos bateaux a été touché
                       dico_bateaux={'bibonhomme':2,'tribonhomme1':3,'tribonhomme2':3,'quadribonhomme':4,'pentabonhomme':5}
                       dico_bateaux_adversaire={'bibonhomme':2,'tribonhomme1':3,'tribonhomme2':3,'quadribonhomme':4,'pentabonhomme':5}



                       #completage des dictionnaires de touchée
                       for i in range(len(grille_joueur)):
                           for j in range(len(grille_joueur[i])):
                               grille_joueur_touchee[grille_joueur[i][j]]=[0,None]

                       for i in range(len(grille_adversaire)):
                           for j in range(len(grille_adversaire[i])):
                               grille_adversaire_touchee[grille_adversaire[i][j]]=[0,None]


                       grille_adversaire_attaque={}
                       grille_joueur_attaque={}
                       
                       ###################
                       #Variables de jeu
                       ###################
                       placement_adversaire=True #Boucle de placement adversaire
                       numero_bateaux=1 #Pour le placement
                       placement_bateaux=0 #Confirmation
                       placement_bateaux_adversaire=0
                       sens=0
                       verif=None #Si placement valable
                       tempo=0 #Compteur 4 tours
                       increment_attaque=0 #Différenciation des couleurs pour la pose des cases attaqués
                       score_joueur=0 #Score joueur par bateau coulé
                       score_adversaire=0 #Score adversaire par bateau coulé
                       prochain_coup=[] #Indication IA pour guider les coups
                       score_init_adversaire=0
                       tour_compte=0#Compteur tour


                       #cette variable sert à savoir si c'est le tour du joueur
                       # ou de l'adversaire ou si le jeu n'a pas encore commencé
                       tour="joueur"
                       jeu='placement'
                       
                       #Voyants
                       #Bibonhomme
                       bi_ind=pygame.image.load("graphics/etat/2/2.png")
                       bi_ind_rect=bi_ind.get_rect(bottomright=(50,485))
                       #Tri
                       tri1_ind=pygame.image.load("graphics/etat/3/3.png")
                       tri1_ind_rect=tri1_ind.get_rect(bottomright=(100,485))

                       tri2_ind=pygame.image.load("graphics/etat/3/3.png")
                       tri2_ind_rect=tri2_ind.get_rect(bottomright=(150,485))
                       #Quadri
                       qua_ind=pygame.image.load("graphics/etat/4/4.png")
                       qua_ind_rect=qua_ind.get_rect(bottomright=(200,485))
                       #Penta
                       pent_ind=pygame.image.load("graphics/etat/5/5.png")
                       pent_ind_rect=pent_ind.get_rect(bottomright=(250,485))
                       
                       theme.stop()
                       theme.play()
#%%
           
           #Impressions des deux boutons de choix du menu de fin
           screen.blit(rematch,rematch_rect)
           screen.blit(sortie2,sortie2_rect)
           
           
       pygame.display.update()
       #Compteur animation radar
       if tempo==240:
           tempo=0
       else:
           tempo+=1
       #choix du framerate
       clock.tick(60)



