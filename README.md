# Aventure du kayak JO 2024

## installation sur votre maschine

### pré-requis

-Nous vous invitons à regarder le fichier requirements.txt pour connaitre les pré-requis 
pour pouvoir installer le projet

-Il est fortement conseillé d'utiliser le programme sur windows, il peut probablemment fonctionner sur linux mis aucune vérification n'ont été faite, idem pour macOS

### installation du projet

pour installer le projet, vous allez devoir entrer deux commandes dans git bash :

-premièrement entrez la commande suivant :

 cd C:\\Users\\user

Si la commande ne fonctionne pas, essayez la avec une 
autre lettre qu'un "C", elle peut varier selon les différent ordinateur ("D","F"...)

-Ensuite vous devrez entrer dans git bash la commande suivante : 

git clone https://gitlab.com/berangerlamo/aventure-du-kayak-jo-2024.git

Désormais le projet est installé sur votre maschine, vous pourrez retrouver les différents éléments du projet en allant dans votre explorateur de fichier dans ce pc, User, user, aventure-du-kayak-jo-2024.
----
Toutefois, si cela n'est pas fonctionnel ou disponible, il est aussi possible de telecharger sous format zip via Gitlab et d'extraire le fichier.

## lancement du jeu

pour lancer le jeu L'aventure du kayak, vous devez ouvrir l'invite de commande de votre maschine retrouvable facilement en tapant "cmd" dans votre barre de recherche windows. Ensuite, vous n'aurrez qu'à entrer la commande suivante :

python aventure-du-kayak-jo-2024\sources\main.py

## si le jeu ne se lance pas

Si vous ne vous n'arrivez pas à lancer le jeu avec l'invite de commande, il existe tous de même un autre moyen de le lancer.

Pour cela, il vous faudra retrouver dans les dossier "ce PC/User/user/aventure-du-kayak-jo-2024/sources" le fichier nommé "main.py". Vous devrez ouvrir ce fichier dans un logiciel de traitement de code comme Spyder ou Visual Studio Code et lancer le programme depuis le logiciel